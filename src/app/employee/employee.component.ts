import { Component, OnInit } from '@angular/core';
import { Form, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { EmployeeService } from '../services/employee.service';

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.css'],
  providers: [FormBuilder]
})
export class EmployeeComponent implements OnInit {
  departments = [];
  departmentForm : FormGroup;
  employeeForm : FormGroup;

  constructor(private employeeService: EmployeeService, private formBuilder: FormBuilder) { 

  }

  ngOnInit() {

   this.departmentForm = this.formBuilder.group({
      name: ["", Validators.required]
   });

  //  this.employeeForm = this.formBuilder.group({
  //     name: ["",Validators.required],
  //     type: ["", Validators.required],
  //     department: [0, Validators.required],
  //     reportingTo: ["", Validators.required]
  //  });

   this.setDepartments();
   this.getDepartments();
   this.setEmployee();
  }


  setDepartments(){
    var departmentList = [{id:1, name:"IT"}, {id:2, name:"HR"}, {id:3, name:"Operation"}]
    localStorage.setItem("departments", JSON.stringify(departmentList));
  }

  getDepartments(){
    this.departments = this.employeeService.getDepartments();
  }

  setEmployee(){
    var employees = [
      {name: "Rajesh",type: "Manager",department: 1,reportingTo : 0 },
      { name: "Kiran", type: "Worker", department: 1, reportingTo : "Rajesh" },
      { name: "Sumit", type: "Worker", department: 1, reportingTo : "Rajesh" },
      { name: "Mansi", type: "Worker", department: 3, reportingTo : "Rajesh" },
      { name: "Rohit", type: "Manager", department: 2, reportingTo : "Rajesh" },
      { name: "Ronak", type: "Worker", department: 2, reportingTo : "Rohit" },
      { name: "Krutika", type: "Manager", department: 3, reportingTo : "Rajesh" }
    ]

    localStorage.setItem("employees", JSON.stringify(employees));
  }


  getEmployees(){
      var employees = JSON.parse(localStorage.getItem("employees"));
      var formatedObj = [];
      employees.forEach(emp => {
          var obj = {
              name: emp.name,
              type: emp.type,
              department: emp.department
          }

          if(emp.type === "Manager"){
            var reportingToEmp = employees.filter(empObj =>{
                return empObj.reportingTo = emp.name
            })

            obj["reporties"] = reportingToEmp;
          }

          formatedObj.push(obj);
      });
      
      
  }

  submitDepartment(){
      var formValues = this.employeeForm.value;
      console.log(formValues);
  }
}
